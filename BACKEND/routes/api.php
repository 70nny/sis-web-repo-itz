<?php

Route::group([

    'middleware' => 'api',
    // 'prefix' => 'auth'

], function () {

    Route::post('login', 'AuthController@login');
    Route::post('registrar', 'AuthController@registrar');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

    Route::post('registrar-articulo', 'Articulos@store');
    Route::get('mostrar-articulos', 'Articulos@index');
    Route::get('show-articulo/{id}', 'Articulos@show');
    Route::patch('update-articulo/{id}', 'Articulos@update');
    Route::delete('borrar-articulo/{id}', 'Articulos@destroy');
    Route::get('buscar', 'Articulos@search');


});
