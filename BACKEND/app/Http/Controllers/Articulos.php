<?php

namespace App\Http\Controllers;

use App\Articulo;
use Illuminate\Support\Facades\DB;
use Request;

class Articulos extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $articulos = DB::table('articulos')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Articulo::create(Request::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Articulo::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $articulo = Articulo::find($id);
        $articulo->titulo = Request::get('titulo');
        $articulo->autor  = Request::get('autor');
        $articulo->categoria = Request::get('categoria');
        $articulo->fecha = Request::get('fecha');
        $articulo->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Articulo::destroy($id);
    }

    public function search(Request $request) {
        $result = Articulo::search(trim($request->search))
            ->autor($request->autor)
            ->categoria($request->tesis)
            ->categoria($request->articulo)->paginate(10);

        return response()->json(['busqueda'=>$result]);
    }
}
