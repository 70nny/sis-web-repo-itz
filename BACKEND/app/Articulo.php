<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    protected $table = 'articulos';
    protected $guarded = [];  


    public function scopeSearch($query, $searchText) {
        if($searchText) {
            return $query->where('titulo', 'like', "%$searchText%");
        }
    }

    public function scopeAutor($query, $searchText) {
        if($searchText) {
            return $query->where('autor', 'like', "%$searchText%");
        }
    }

    public function scopeTesis($query) {
        return $query->where('categoria', '=', "Tesis");
    }

    public function scopeArticulo($query) {
        return $query->where('categoria', '=', "Articulo");
    }

}
