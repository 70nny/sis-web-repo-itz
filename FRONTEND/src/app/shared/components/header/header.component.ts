import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/services/token.service';
import { JarwisService } from 'src/app/services/jarwis.service';
import { FormGroup, FormBuilder} from '@angular/forms';
import { MsjSweetService } from 'src/app/services/msj-sweet.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public loggedIn: boolean
  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter()

  formSearch: FormGroup
    public error = []
  
   constructor(private Auth: AuthService,
    private router: Router,
    private Token: TokenService,
    private fb: FormBuilder,
    private Jarwis: JarwisService,
    private msj: MsjSweetService) { }

  ngOnInit() {
    this.formSearch = this.fb.group({
      titulo: [null, null ],
      autor: [null, null],
      categoria: [null, null],  
    })
  }

  search() {

  }

  toggleSideBar() {
    this.toggleSideBarForMe.emit()
    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      )
    }, 300)
  }

  logout(event: MouseEvent) {
    event.preventDefault()
    this.Token.remove()
    this.router.navigateByUrl('/login')
  }
}
