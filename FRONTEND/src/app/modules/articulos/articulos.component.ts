import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { JarwisService } from 'src/app/services/jarwis.service';
import { MsjSweetService } from 'src/app/services/msj-sweet.service';
import { TokenService } from 'src/app/services/token.service';
import { reportArticulos } from 'src/reportArticulos';
import Swal from 'sweetalert2'



@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.scss']
})

export class ArticulosComponent implements OnInit {

  id: number;
  ELEMENT_DATA : reportArticulos[];
  displayedColumns: string[] = ['id','titulo', 'autor', 'categoria', 'fecha', 'acciones'];
  dataSource = new MatTableDataSource<reportArticulos>();
  error: any;

  constructor(private service:JarwisService, 
    private router:Router,
    public dialog: MatDialog,
    private msj: MsjSweetService,
    private Token: TokenService,){}

  ngOnInit(){
    this.getArticulos();
  }

  
  public getArticulos(){
    let resp = this.service.showArticulos();
    resp.subscribe(report=>this.dataSource.data=report as reportArticulos[]);
  }
  
  
  borrar(id){
    Swal.fire({
      title: '¿Eliminar Registro?',
      text: "Esta acción no se puede reverir?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.delArticulo(id).subscribe(x => {
          data => this.handleResponse(data)
          this.getArticulos()
          this.msj.msjToastCorrecto("Proyecto eliminado!")
    
        }, err => {
            const validationErrors = err.error.errors;
            console.log(err.error.errors)
            if (err instanceof HttpErrorResponse) {
              const errorMessages = new Array<{ propName: string; errors: string }>();
        
              if (err.status === 422) {
                this.msj.msjToastError("Error al eliminar!")
              }
            }
          }
        )
        Swal.fire(
          'Proyecto eliminado',
          'El registro ha sido eliminado.',
          'success'
        )
      }
    })


   
  }

  editar(id){
    this.router.navigateByUrl('/editar-articulo')
  }

  nuevo() {
    this.router.navigateByUrl('/registrar-articulo')  
  
  }

  handleResponse(data) {
    this.Token.handle(data.access_token)
    this.router.navigateByUrl('/articulos')
  }
  errorRequest(error) {    
    console.log(error.error.errors)
    this.msj.msjToastErrorClose(JSON.stringify((this.error = error.error.errors)))
  }

}
  
  