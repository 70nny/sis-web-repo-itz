import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { MatPaginator } from '@angular/material';
import { JarwisService } from 'src/app/services/jarwis.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  bigChart = []
  cards = []
  pieChart = []
  data

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  
  constructor(private dashboardService: DashboardService,
    private jarwis: JarwisService) { }

  ngOnInit() {
    this.bigChart = this.dashboardService.bigChart()
    this.cards = this.dashboardService.cards()
    this.pieChart = this.dashboardService.pieChart()
    this.getArticulos()
  }

  public getArticulos(){
    this.jarwis.showArticulos().subscribe(data => {
      this.data = data
    })
  
  }

}
