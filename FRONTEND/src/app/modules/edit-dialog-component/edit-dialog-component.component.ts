import { Component, OnInit } from '@angular/core';
import { ErrorStateMatcher } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { MsjSweetService } from 'src/app/services/msj-sweet.service';
import { FormControl, FormGroupDirective, NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';
import { ActivatedRoute, Router } from '@angular/router';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-edit-dialog-component',
  templateUrl: './edit-dialog-component.component.html',
  styleUrls: ['./edit-dialog-component.component.scss']
})
export class EditDialogComponentComponent implements OnInit {
  formularioProyecto: FormGroup
  public error = []
  data: any = {};

  constructor(private fb: FormBuilder,
    private Jarwis: JarwisService,
    private msj: MsjSweetService,
    private Token: TokenService,
    private ruta: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.formularioProyecto = this.fb.group({
      titulo: [null, Validators.required ],
      autor: [null, Validators.required],
      categoria: [null, Validators.required],
      fecha: [null, Validators.required]   
    })

    this.getArticulo();
  }

  matcher = new MyErrorStateMatcher
  
  public hasError = (controlName: string, errorName: string) => {
    return this.formularioProyecto.controls[controlName].hasError(errorName);
  }
  
  public getArticulo(){
    this.Jarwis.showArticulo(this.ruta.snapshot.params.id).subscribe(x => {
      this.data = x
      this.formularioProyecto.setValue({
        titulo: this.data.titulo,
        autor: this.data.autor,
        categoria: this.data.categoria,
        fecha: this.data.fecha
      }) 
      
    }, err => {
        const validationErrors = err.error.errors;
        console.log(err.error.errors)
        if (err instanceof HttpErrorResponse) {
          const errorMessages = new Array<{ propName: string; errors: string }>();
    
          if (err.status === 422) {
            this.msj.msjToastError("Error al eliminar!")
          }
        }
      }
    )
  }

  update() {
    this.Jarwis.updArticulo(this.ruta.snapshot.params.id, this.formularioProyecto.value).subscribe(x => {
      data => this.handleResponse(data)
      this.msj.msjToastCorrecto("El proyecto actualizado!")
      this.router.navigateByUrl('/articulos')

    }, err => {
        const validationErrors = err.error.errors;
        console.log(err.error.errors)
        if (err instanceof HttpErrorResponse) {
          const errorMessages = new Array<{ propName: string; errors: string }>();
    
          if (err.status === 422) {
            this.msj.msjToastError("Error al registrar!")
            Object.keys(validationErrors).forEach(prop => {
              const formControl = this.formularioProyecto.get(prop);
              if (formControl) {
                // Activa los msjs de error
                formControl.setErrors({
                  serverError: validationErrors[prop]
                });
              }
            });
          }
        }
      }
    )
  }

  cancelar(){
    this.router.navigateByUrl('/articulos')
  }

  handleResponse(data) {
    this.Token.handle(data.access_token)
    this.router.navigateByUrl('/articulos')
  }
  errorRequest(error) {    
    console.log(error.error.errors)
    this.msj.msjToastErrorClose(JSON.stringify((this.error = error.error.errors)))
  }

}
