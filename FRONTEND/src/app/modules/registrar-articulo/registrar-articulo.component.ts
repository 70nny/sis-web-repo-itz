import { Component, OnInit } from '@angular/core';
import { ErrorStateMatcher } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { MsjSweetService } from 'src/app/services/msj-sweet.service';
import { FormControl, FormGroupDirective, NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';
import { Router } from '@angular/router';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-registrar-articulo',
  templateUrl: './registrar-articulo.component.html',
  styleUrls: ['./registrar-articulo.component.scss']
})

export class RegistrarArticuloComponent implements OnInit {
    formularioProyecto: FormGroup
    public error = []
  
    constructor(private fb: FormBuilder,
      private Jarwis: JarwisService,
      private msj: MsjSweetService,
      private Token: TokenService,
      private router: Router
    ) { }
  
    ngOnInit() {
      this.formularioProyecto = this.fb.group({
        titulo: [null, Validators.required ],
        autor: [null, Validators.required],
        categoria: [null, Validators.required],
        fecha: [null, Validators.required]   
      })
    }
  
    matcher = new MyErrorStateMatcher
    
    public hasError = (controlName: string, errorName: string) => {
      return this.formularioProyecto.controls[controlName].hasError(errorName);
    }
  
    registrar() {
      this.Jarwis.regArticulo(this.formularioProyecto.value).subscribe(x => {
        data => this.handleResponse(data)
        this.msj.msjToastCorrecto("Proyecto registrado exitosamente!")
        this.router.navigateByUrl('/articulos')

        //AGREGAR AQUI REDIRECCION A PAGINA DE USUARIOS
      }, err => {
          const validationErrors = err.error.errors;
          console.log(err.error.errors)
          if (err instanceof HttpErrorResponse) {
            const errorMessages = new Array<{ propName: string; errors: string }>();
      
            if (err.status === 422) {
              this.msj.msjToastError("Error al registrar!")
              Object.keys(validationErrors).forEach(prop => {
                const formControl = this.formularioProyecto.get(prop);
                if (formControl) {
                  // Activa los msjs de error
                  formControl.setErrors({
                    serverError: validationErrors[prop]
                  });
                }
              });
            }
          }
        }
      )
    }

    cancelar(){
      this.router.navigateByUrl('/articulos')
    }
  
    handleResponse(data) {
      this.Token.handle(data.access_token)
      this.router.navigateByUrl('/articulos')
    }
    errorRequest(error) {    
      console.log(error.error.errors)
      this.msj.msjToastErrorClose(JSON.stringify((this.error = error.error.errors)))
    }
  
  }
