import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JarwisService {

  private baseUrl = 'http://localhost:8000/api/'
  constructor(private http: HttpClient) { }
  
  login(data) {
    return this.http.post(this.baseUrl + 'login', data)
  }
  registrar(data) {
    return this.http.post(this.baseUrl + 'registrar', data)
  }

  regArticulo(data) {
    return this.http.post(this.baseUrl + 'registrar-articulo', data)
  }

  showArticulo(id) {
    return this.http.get(this.baseUrl + 'show-articulo/'+id)
  }

  updArticulo(id, data) {
    return this.http.patch(this.baseUrl + 'update-articulo/'+id, data)
  }

  delArticulo(id) {
    return this.http.delete(this.baseUrl + 'borrar-articulo/'+id)
  }

  showArticulos() {
    return this.http.get(this.baseUrl + 'mostrar-articulos')
  }
}

