import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import { DashboardComponent } from 'src/app/modules/dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { PostsComponent } from 'src/app/modules/posts/posts.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatSelectModule, MatNativeDateModule, MatDatepickerModule, MatSidenavModule, MatDividerModule, MatCardModule, MatPaginatorModule, MatTableModule, MatFormFieldModule, MatIconModule, MatInputModule, MatButtonModule } from '@angular/material'
import { FlexLayoutModule } from '@angular/flex-layout';
import { DashboardService } from 'src/app/modules/dashboard.service';
import { PerfilComponent } from 'src/app/modules/perfil/perfil.component';
import { RequestResetComponent } from 'src/app/modules/request-reset/request-reset.component';
import { ResponseResetComponent } from 'src/app/modules/response-reset/response-reset.component';
import { LoginComponent } from 'src/app/modules/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegistrarUsuariosComponent } from 'src/app/modules/registrar-usuarios/registrar-usuarios.component';
import { ArticulosComponent } from 'src/app/modules/articulos/articulos.component';
import { RegistrarArticuloComponent } from 'src/app/modules/registrar-articulo/registrar-articulo.component';
import { EditDialogComponentComponent } from 'src/app/modules/edit-dialog-component/edit-dialog-component.component';
import { BusquedaComponent } from 'src/app/modules/busqueda/busqueda.component';



@NgModule({
  declarations: [
    DefaultComponent,
    DashboardComponent,
    PostsComponent,
    PerfilComponent,
    LoginComponent,
    RequestResetComponent,
    ResponseResetComponent,
    RegistrarUsuariosComponent,
    ArticulosComponent,
    RegistrarArticuloComponent,
    EditDialogComponentComponent,
    BusquedaComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,
    MatDividerModule,
    FlexLayoutModule,
    MatCardModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    DashboardService
  ]
})
export class DefaultModule { }
